<?php
/**
 * @package robusta
 */
/*
Plugin Name: robusta
plugin URI: http://robustastudio.com/
Description: This is custom plugin to display custom table in the admin page
Version: 1.0.0
Author: Mohamed Adel
Authour URI: http://madel.com
License: GPLv2 or later
Text Domain: robusta
*/

if(!defined('ABSPATH')){
    define('ABSPATH', dirname(__FILE__) . '/');
}

//include database file
include_once('dbfile.php');

//register hook
register_activation_hook(__FILE__, "DBP_tb_create");

add_action('admin_menu', 'robusta_plugin_setup_menu');
wp_register_style( 'robusta-style', plugins_url( 'style.css', __FILE__  ) );
wp_enqueue_style( 'robusta-style' );
 
function robusta_plugin_setup_menu(){
        add_menu_page( 'robusta Plugin Page', 'robusta Plugin', 'manage_options', 'robusta-plugin', 'robusta_init' );
}
 
function robusta_init(){
        require_once("display_data.php");
}

?>
