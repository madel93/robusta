<?php
/*
Creating DataBase Table For Plugin
*/
 function DBP_tb_create(){
    global $wpdb;

    //step 1
    $DBP_tb_name=$wpdb->prefix ."role_table";

    //step 2
    $DBP_query="CREATE TABLE $DBP_tb_name(
        id int (10) NOT NULL AUTO_INCREMENT,
        name varchar (100) DEFAULT '',
        role varchar (100) DEFAULT '',
        PRIMARY KEY (id)
     )";
   //step 3
    require_once(ABSPATH ."wp-admin/includes/upgrade.php");
    dbDelta($DBP_query);
 }
?>